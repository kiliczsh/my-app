FROM node:8 AS build-env

WORKDIR /my-app

ENV PATH /my-app/node_modules/.bin:$PATH

COPY . .

RUN yarn install
RUN yarn build
RUN yarn global add serve

EXPOSE 5000
CMD ["serve", "-s", "build"]
